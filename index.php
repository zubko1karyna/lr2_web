<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Website Parfume</title>
    <link href="css/style.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  </head>
  <body>
  <header>
      <?php require "blocks/header.php"?>
  </header>
<div class="container"> 
  <h2 class="mb-5 annonced"> our new drop </h2>
    <div class="d-flex flex-wrap"> <!Отображение блоков>
      <?php 
        for($i=0;$i<6;$i++): 
      ?>
      <div class="card mb-4 shadow-sm">
        <div class="card-body">
          <img src="img/<?php echo ($i + 1) ?>.jpg" class="img-thumbnail"><!Добавление картинок>
          <ul class="list-unstyled mt-3 mb-4">
            <?php 
            switch( $i + 1)
        {
            case 1: include 'pages/1.php' ; break;
            case 2: include 'pages/2.php' ; break;
            case 3: include 'pages/3.php' ; break;
            case 4: include 'pages/4.php' ; break;
            case 5: include 'pages/5.php' ; break;
            case 6: include 'pages/6.php' ; break;
        }
             ?>
          </ul>
          <button type="button" class="btn btn-lg btn-block btn-outline-primary">Details</button>
        </div>
      </div>
      <?php endfor; ?> <!Закрываем цикл >
    </div>
</div>
    <?php require "blocks/footer.php"?>
  </body>
</html>