<footer class="container pt-4 my-md-5 pt-md-5 border-top">
    <div class="row">
      <div class="col-12 col-md">
      <img class="mb-2" src="img/logo1.svg" alt="" width="80" height="80">
      </div>
      <div class="col-6 col-md">
        <h5>Features</h5>
        <ul class="list-unstyled text-small">
          <li><a class="text-muted" href="#">Shipping</a></li>
          <li><a class="text-muted" href="#">Sales</a></li>
          <li><a class="text-muted" href="#">Ambassadors</a></li>
        </ul>
      </div>
      <div class="col-6 col-md">
        <h5>Producing</h5>
        <ul class="list-unstyled text-small">
          <li><a class="text-muted" href="#">Resources</a></li>
          <li><a class="text-muted" href="#">Countries</a></li>
        </ul>
      </div>
      <div class="col-6 col-md">
        <h5>About</h5>
        <ul class="list-unstyled text-small">
          <li><a class="text-muted" href="#">Team</a></li>
          <li><a class="text-muted" href="#">Locations</a></li>
          <li><a class="text-muted" href="#">Privacy</a></li>
        </ul>
      </div>
    </div>
  </footer>