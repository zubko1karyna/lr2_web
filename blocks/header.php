<div class="d-flex flex-column flex-md-row align-items-center p-3 px-md-4 mb-3 bg-white border-bottom shadow-sm">
   <img class="my-0 mr-md-auto font-weight-normal" src="img/logo1.svg" alt="Parfume" width="80" height="80">
  <nav class="my-2 my-md-0 mr-md-3">
    <a class="p-2 text-dark" href="index.php">Home</a>
    <a class="p-2 text-dark" href="contact.php">Contact</a>
  </nav>
  <a class="btn btn-outline-primary" href="#">Log In</a>
</div>